CHANGELOG
===================

Ce fichier est basé sur [Keep a Changelog](http://keepachangelog.com/)
et le projet utilise [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.2.1] - 2020-10-07

### Corrections

- Les propriétés fonctionnelles objet sont maintenant automatiquement configurées à 'single'

## [1.2.0] - 2019-01-14

### Modification

- Modification de l'IRI des ontologies DATAtourisme

## [1.1.1] - 2018-08-20

### Corrections

- Meilleur cohérence entre le menu principal des POI et la liste des
propriétés à aligner

## [1.1.0] - 2018-06-04

### Ajouts

- Support de l'alignement multilingue pour les propriétés range =
rdfs:langString

## [1.0.1] - 2018-02-27

### Corrections

- L'import sur PointOfInteret fonctionne maintenant correctement

## [1.0.0] - 2018-02-12

### Ajouts

- Ajout de l'opération atomique "html2text"
- Tri des propriétés par priorité + signalétique particulière pour les
priorités > 5
- Prise en compte des priorités dans l'ordre des propriétés dans la preview
- Procédure d'import/export des règles

### Modifications
- Modification du format d'alignement
- Browserify

### Corrections

- Affichage des erreurs HTTP dans une notification bootstrap
- L'édition des arguments d'un traitement atomique fonctionne maintenant
correctement (mantis)

--------------------

## [0.1.1] - 2017-07-04

### Ajouts

- Interface d'alignement
- Interface de prévisualisation
- Éditeur de chaîne de traitements atomiques (+champ input)