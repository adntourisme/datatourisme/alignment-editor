/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

// module definition
angular.module('alignment-editor')
    .controller('NavigatorController', NavigatorController);

/**
 * @ngInject
 */
function NavigatorController($scope, $stateParams, entrypoint) {
    $scope.$root.path = $stateParams.path;
    $scope.$root.entrypoint = entrypoint;
}