/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

// module definition
angular.module('alignment-editor')
    .config(config);

/**
 * @ngInject
 */
function config($stateProvider, appConfig) {
    $stateProvider
        .state('app.navigator_uri', {
            url: "/:uri",
            params: {
                uri: { value: appConfig.datatourisme.namespace + 'PointOfInterest' }
            },
            resolve: {
                entrypoint: resolveEntrypoint,
                xpathSchema: resolveXpathSchema
            },
            template: "<div ui-view></div>",
            controller: function($rootScope, xpathSchema) {
                $rootScope.xpathSchema = xpathSchema;
            },
            abstract: true
        })
        .state('app.navigator', {
            url: "/{path:.*}",
            parent: 'app.navigator_uri',
            params: {
                path: { value: null, squash: true }
            },
            views: {
                '': {
                    template: "<div></div>",
                    controller: 'NavigatorController'
                },
                'breadcrumb@': {
                    template: require("./navigator.breadcrumb.html"),
                    controller: "NavigatorBreadcrumbController"
                }
            }
        });
}

/**
 * @ngInject
 */
function resolveXpathSchema(ontology, apiEndpoints, $stateParams) {
    var _class = ontology.getClass($stateParams.uri);
    if(_class) {
        return apiEndpoints.xpath.load(_class.getShortUri());
    }
}

/**
 * @ngInject
 */
function resolveEntrypoint(ontology, $stateParams) {
    return ontology.getClass($stateParams.uri);
}