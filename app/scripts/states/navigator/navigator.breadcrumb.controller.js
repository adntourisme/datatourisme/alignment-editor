/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

// module definition
angular.module('alignment-editor')
    .controller('NavigatorBreadcrumbController', NavigatorBreadcrumbController);

/**
 * @ngInject
 */
function NavigatorBreadcrumbController($scope, $stateParams, ontology) {
    $scope.ontology = ontology;
    $scope.root = ontology.getClass($stateParams.uri);
    var path = $stateParams.path;
    var parts = [];
    if(path) {
        parts = path.split("/");
        for(var i=0; i<parts.length; i++) {
            parts[i] = {
                path: path.split("/").slice(0, i+1).join("/"),
                property: ontology.getProperty(parts[i])
            };
        }
    }
    $scope.path = parts;
}