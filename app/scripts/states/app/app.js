/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

// module definition
angular.module('alignment-editor')
    .config(config);

/**
 * @ngInject
 */
function config($stateProvider, appConfig) {
    $stateProvider.state('app', {
        abstract: true,
        template: require("./app.html"),
        controller: "AppController",
        resolve: {
            ontology: resolveOntology,
            definition: resolveDefinition,
            stats: resolveStats
        }
    });
}

/**
 * @ngInject
 */
function resolveOntology(apiEndpoints) {
    return apiEndpoints.ontology.load();
}

/**
 * @ngInject
 */
function resolveDefinition(apiEndpoints, ontology) {
    return apiEndpoints.alignment.load(ontology);
}

/**
 * @ngInject
 */
function resolveStats(apiEndpoints) {
    return apiEndpoints.stats.stats();
}