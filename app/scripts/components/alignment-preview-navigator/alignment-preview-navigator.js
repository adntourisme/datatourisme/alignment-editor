/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('alignment-editor')
    .component('alignmentPreviewNavigator', {
        template: require("./alignment-preview-navigator.html"),
        controller: controller,
        bindings: {
            "definition": "<",
            "type": "<"
        }
    });

/**
 * @ngInject
 */
function controller($scope, appConfig, apiEndpoints, $sessionStorage) {
    var ctrl = this;
    var endpoint = apiEndpoints.preview;
    var storage;

    this.data = null;
    this.item = null;
    this.preview = null;
    this.loading = {
        index: true,
        preview: true
    };

    this.$onInit = function () {
        if(this.definition) {
            this.init();
        } else {
            apiEndpoints.alignment.load().then(function(definition) {
                ctrl.definition = definition;
                ctrl.init();
            });
        }
    };

    this.init = function () {
        var type = this.type || appConfig.datatourisme.namespace + 'PointOfInterest';

        // init storage
        var key = appConfig.endpoints.alignment;
        storage = $sessionStorage[key];
        if(!storage) {
            storage = $sessionStorage[key] = {};
        }
        if(!storage[type]) {
            storage[type] = {page: 1, selected: null};
        }
        storage = storage[type];

        // normalize type
        var parts = type.split(/[\/#:]/);
        type = parts[parts.length-1];

        // init params
        this.params = {
            page: storage.page,
            type: type,
            limit: 15
        };

        // watch params
        $scope.$watchCollection(function() {
            return ctrl.params;
        }, function() {
            ctrl.loading.index = true;
            endpoint.index(ctrl.params).then(function(data) {
                ctrl.data = data;
                ctrl.loading.index = false;
                storage.page = ctrl.params.page;
                if(storage.selected)  {
                    var item = getItemById(storage.selected);
                    if(item) {
                        ctrl.select(item);
                        return;
                    }
                }
                if(data.items.length) {
                    ctrl.select(data.items[0]);
                }
            }, function(data) {
                console.error(data);
            });
        });
    };


    this.select = function(item) {
        if(this.definition) {
            ctrl.item = item;
            ctrl.loading.preview = true;
            endpoint.preview(item.id, this.definition.toJson()).then(function(data) {
                ctrl.preview = data;
                ctrl.loading.preview = false;
                storage.selected = item.id;
            }, function(data) {
                console.error(data);
            });
        }
    };

    this.totalPage = function() {
        if(this.data) {
            return Math.ceil(this.data.total/this.params.limit);
        }
    };

    this.prevPage = function() {
        if(this.params.page > 1 && !this.loading.index) {
            this.params.page--;
        }
    };

    this.nextPage = function() {
        if(this.params.page < this.totalPage() && !this.loading.index) {
            this.params.page++;
        }
    };

    function getItemById(id) {
        if(ctrl.data.items) {
            for(var i=0; i<ctrl.data.items.length; i++) {
                if(ctrl.data.items[i].id == id) {
                    return ctrl.data.items[i];
                }
            }
        }
        return false;
    }
}