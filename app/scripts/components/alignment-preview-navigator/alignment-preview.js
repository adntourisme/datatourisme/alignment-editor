/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('alignment-editor')
    .component('alignmentPreview', {
        template: require("./alignment-preview.html"),
        controller: controller,
        bindings: {
            "data": "<"
        }
    });

/**
 * @ngInject
 */
function controller() {
    var ctrl = this;
    this.resource = null;
    this.propertyFilter = 'mapped';

    this.$onChanges = function () {
        if(this.data) {
            this.resource = this.data.resource;
        }
    };

    /**
     * Function to determine property order
     *  1: errors
     *  2: warning
     *  3: label/uri
     * @param property
     * @returns {*}
     */
    this.orderProperty = function(property) {
        var order = (property.priority ? property.priority : 0);
        if(property.anomalies) {
            for(var i=0; i<property.anomalies.length; i++) {
                if(property.anomalies[i].error) {
                    order = "2_" + order;
                } else {
                    order = "1_" + order;
                }
            }
        } else if((property.values && property.values.length) || property.mapped) {
            order = "0_" + order;
        }
        return order;
    };

    this.filterProperty = function(property) {
        if(ctrl.propertyFilter) {
            var status = ctrl.getPropertyStatus(property);
            switch(ctrl.propertyFilter) {
                case "mapped":
                    return (status != "unmapped");
                case "anomaly":
                    if(["error", "warning"].indexOf(status) > -1) {
                        return true;
                    }
                    if(property.values) {
                        for(var i=0; i<property.values.length; i++) {
                            if(property.values[i].properties) {
                                for(var j=0; j<property.values[i].properties.length; j++) {
                                    if(ctrl.filterProperty(property.values[i].properties[j])) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                    return false;
            }
        }
        return true;
    }

    /**
     * Get the property status
     *
     * @param property
     * @returns {*}
     */
    this.getPropertyStatus = function(property) {
        if(property.anomalies) {
            for(var i=0; i<property.anomalies.length; i++) {
                if(property.anomalies[i].error) {
                    return "error";
                }
            }
            return "warning";
        }
        if(!property.values || !property.values.length) {
            if(property.mapped) {
                return "mapped";
            }
            return "unmapped";
        }
        return "success";
    }


    /**
     * Get the property status
     *
     * @param property
     * @returns {*}
     */
    this.getReports = function(error) {
        var reports = [];
        if(this.data && this.data.report.reports) {
            for(var i=0; i<this.data.report.reports.length; i++) {
                var report = this.data.report.reports[i];
                if(report.error == error) {
                    reports.push(report);
                }
            }
        }
        return reports;
    }
}