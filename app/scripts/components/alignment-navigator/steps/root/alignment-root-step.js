/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

angular
    .module('alignment-editor')
    .component('alignmentRootStep', {
        template: require("./alignment-root-step.html"),
        require: {
            navigator: "^alignmentNavigator"
        },
        controller: controller,
        bindings: {
            "entrypoint": "<"
        }
    });

/**
 * @ngInject
 */
function controller() {
    var ctrl = this;
    var ontology;
    this.steps = [];

    this.$onChanges = function () {
        // onchange, populate steps
        if(this.entrypoint) {
            ontology = ctrl.navigator.ontology;
            this.steps = getRootSteps(this.entrypoint);
        }
    };

    function getRootSteps(_class) {
        var step = {
            'classes': [_class],
            'steps': ctrl.navigator.getSteps([_class])
        };
        // OLD CODE : include super classes steps
        /*if(_class.superClasses) {
         for(var i=0; i<_class.superClasses.length; i++) {
         var superClass = _class.superClasses[i];
         if(ontology.isSubclassOf(superClass, ontology.PointOfInterest)) {
         steps = steps.concat(getRootSteps(superClass));
         }
         }
         }*/
        return [step];
    }
}