/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('alignment-editor')
    .component('alignmentStep', {
        template: require("./alignment-step.html"),
        controller: controller,
        require: {
            navigator: "^alignmentNavigator"
        },
        bindings: {
            "step": "<"
        }
    });

/**
 * @ngInject
 */
function controller(bsNgDialog) {
    var ctrl = this;

    /**
     * Define applicable CSS class for a given (sub)step
     *
     * @param step
     * @returns {Array}
     */
    this.getCSSClasses = function(step) {
        var classes = [];
        classes.push(this.getStepStatus(step).code);
        if(this.navigator.isActiveStep(step)) {
            classes.push('active');
        }
        return classes;
    };

    this.getStepStatus = function(step) {
        var defStatus = this.navigator.getDefinitionStatus(step.path);
        var status = {
            code: "missing",
            message: "Cette propriété n'est pas alignée."
        };
        if(defStatus == true) {
            return {
                code: "success",
                message: "Cette propriété est alignée."
            }
        } else if(defStatus == false) {
            if(step.property.priority > 5) {
                return {
                    code: "important",
                    message: "Cette propriété est importante : il est vivement recommandé de la renseigner pour garantir la qualité de l'information."
                }
            }
        }
        return status;
    };

    /**
     * Used to exclude class URI from domains
     *
     * @param uri
     * @returns {Function}
     */
    this.excludeDomain = function() {
        return function(value) {
            if(!ctrl.step.classes || !ctrl.step.classes.length || ctrl.step.classes.length > 1) {
                return true;
            } else {
                return (value != ctrl.step.classes[0].uri);
            }
        }
    };

    /**
     * Export rules (and subrules) of the current step
     *
     * @param uri
     * @returns {Function}
     */
    this.export = function() {
        var path = this.step.path;
        var rules = this.navigator.getDefinition().getRules(path);

        if(rules) {
            var filename = this.step.classes[0].getUri().split('#')[1] + ".json";
            var data = JSON.stringify({
                "@context": this.navigator.getDefinition().getPrefixMap(),
                "rules": rules
            }, null, 2);

            var blob = new Blob([data], {type: 'application/json'});
            if(window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveBlob(blob, filename);
            }
            else{
                var elem = window.document.createElement('a');
                elem.href = window.URL.createObjectURL(blob);
                elem.download = filename;
                document.body.appendChild(elem);
                elem.click();
                document.body.removeChild(elem);
            }
        }
    };

    /**
     * Import rules (and subrules) for the current step
     *
     * @returns {Function}
     */
    this.import = function() {
        bsNgDialog.open({
            fullHeight: false,
            template: require("./import-dialog/import-dialog.html"),
            plain: true,
            controller: 'ImportDialogController',
            resolve: {
                navigator: function() {
                    return ctrl.navigator;
                },
                step: function() {
                    return ctrl.step;
                }
            }
        });
    };

    /**
     * Deactivate import/export feature for subclasses of PointOfInterest
     *
     * @returns {boolean}
     */
    this.isImportExportEnable = function() {
        if( this.step.classes[0] != this.navigator.ontology.PointOfInterest && this.step.classes[0].isSubClassOf(this.navigator.ontology.PointOfInterest, false)) {
            return false;
        }
        return true;
    };
}