/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('alignment-editor')
    .component('alignmentTypeStep', {
        template: require("./alignment-type-step.html"),
        controller: controller,
        require: {
            navigator: "^alignmentNavigator"
        },
        bindings: {
            step: "<"
        }
    });

/**
 * @ngInject
 */
function controller($controller, $scope) {
    angular.extend(this, $controller('StepFormController', {$scope: $scope}));

    var $ctrl = this;

    this.options = [];
    this.config = {
        searchField: ['label'],
        valueField: 'uri',
        labelField: 'label',
        placeholder: 'Recherchez un type',
        hideSelected: false,
        render: {
            option: function(item, escape) {
                var str = escape(item.label);
                if(!item.depth) {
                    str = "<strong>" + str + "</strong>";
                }
                return '<div>' + Array((item.depth?item.depth:0)+1).join("&nbsp;&nbsp;&nbsp;") + str + '</div>'
            }
        },
        score: function(search) {
            var scoreFunc = this.getScoreFunction(search);
            return function(item) {
                var score = scoreFunc(item);
                if(score || typeChildMatch(item, scoreFunc)) {
                    return 1;
                }
                return 0;
            };
        }
    };

    /**
     * $onChange
     */
    this.init = function () {
        var ontology = this.navigator.ontology;

        // fill options
        var options = [];
        function processClass(ontClass, depth) {
            var subClasses = ontClass.getSubClasses();
            var depth = depth ? depth : 0;
            for(var i=0; i<subClasses.length; i++) {
                options.push({
                    uri: subClasses[i].getUri(),
                    label: subClasses[i].getLabel(),
                    depth: depth,
                    parent: ontClass.getUri()
                });
                processClass(subClasses[i], depth+1);
            }
        }
        processClass(ontology.PointOfInterest);
        this.options = options;
    };

    /**
     * @param item
     * @param scoreFunc
     * @returns {boolean}
     */
    function typeChildMatch(item, scoreFunc) {
        for(var i=0; i<$ctrl.options.length; i++) {
            if($ctrl.options[i].parent == item.uri) {
                if(scoreFunc($ctrl.options[i]) || typeChildMatch($ctrl.options[i], scoreFunc)) {
                    return true;
                }
            }
        }
        return false;
    }
}
