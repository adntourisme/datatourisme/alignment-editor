/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('alignment-editor')
    .component('alignmentLangstringStep', {
        template: require("./alignment-langstring-step.html"),
        controller: controller,
        require: {
            navigator: "^alignmentNavigator"
        },
        bindings: {
            step: "<"
        }
    });

/**
 * @ngInject
 */
function controller($controller, $scope, iso639, $timeout) {
    angular.extend(this, $controller('StepFormController', {$scope: $scope}));

    // load iso
    this.iso639 = iso639;

    // custom def to model
    this.defToModel = function(def) {
        var data = {};
        def = Array.isArray(def) ? def : [def];
        for(var i=0; i<def.length; i++) {
            if(!data.type) {
                data.type = def[i].type == "xpath" || def[i].type == "xquery" ? "extract" : def[i].type;
                data[data.type] = {};
            }
            var lang = def[i].lang ? def[i].lang : 'fr';
            data[data.type][lang] = angular.extend({}, def[i]);
            delete data[data.type][lang].lang;
        }
        return data;
    }

    // custom model to def
    this.modelToDef = function(data) {
        var type = data.type;
        data = angular.copy(data[type]);
        var arr = [];

        for(var lang in data) {
            if(data[lang]) {

                // if constant and no value : abort
                if(type == "constant" && !data[lang].value) {
                    continue;
                }

                arr.push(angular.extend({}, {
                    type: type,
                    lang: lang
                }, data[lang]));
            }
        }

        var val = arr;
        if(!val.length) {
            val = null;
        } else if(val.length == 1) {
            val = val[0];
        }

        return val;
    }
}