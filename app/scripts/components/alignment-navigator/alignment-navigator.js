/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('alignment-editor')
    .component('alignmentNavigator', {
        template: require("./alignment-navigator.html"),
        controller: controller,
        require: {
            ngModelCtrl: 'ngModel'
        },
        bindings: {
            "entrypoint": "<",
            "path": "<",
            "ontology": "<",
            "stats": "<",
            "xpathSchema": "<"
        }
    });

require('./**/*.js', {mode: 'expand'});

/**
 * @ngInject
 */
function controller($state, $timeout, $window, appConfig, bsNgDialog) {
    var ctrl = this;
    this.debug = appConfig.debug;

    this.steps = [];

    this.$onChanges = function () {
        // onchange, populate steps
        // iterate and change to avoid glitches
        //this.steps = pathToSteps(this.path);
        var steps = pathToSteps(this.path);
        for(var i=0; i<steps.length; i++) {
            if(!this.steps[i] || this.steps[i].path !== steps[i].path) {
                this.steps[i] = steps[i];
            }
        }
        this.steps = this.steps.slice(0, steps.length);
        $timeout(fixWrapperOffset, 100);
    };

    /**
     * Get definition (from ngModel)
     *
     * @returns {*}
     */
    this.getDefinition = function() {
        return this.ngModelCtrl.$modelValue;
    }

    // handle window resize
    /*angular.element($window).bind('resize', function() {
        scope.$apply(fixWrapperOffset);
    });*/

    /**
     * Get the next step
     *
     * @param step
     * @returns {null}
     */
    this.getNextStep = function (step) {
        step = normalizeStepArg(step);
        for(var i=0; i<this.steps.length; i++) {
            if(this.steps[i] == step) {
                return this.steps[i+1] ? this.steps[i+1] : null;
            }
        }
        return null;
    };

    /**
     * Get the previous step
     *
     * @param step
     * @returns {null}
     */
    this.getPreviousStep = function (step) {
        step = normalizeStepArg(step);
        for(var i=0; i<this.steps.length; i++) {
            if(this.steps[i] == step) {
                return i > 0 ? this.steps[i-1] : null;
            }
        }
        return null;
    };

    /**
     * Is step active ?
     *
     * @param step
     * @returns {boolean}
     */
    this.isActiveStep = function(step) {
        step = normalizeStepArg(step);
        if(step && this.path) {
            return this.path == step.path || this.path.indexOf(step.path + '/') == 0;
        }
        return false;
    };

    /**
     * Navigate to given step
     *
     * @param path
     */
    this.navigateTo = function(path) {
        if(typeof path == "object" && path != null) {
            path = path.path;
        }
        $state.go('app.navigator', {uri: this.entrypoint.getShortUri(), path: path}); // @todo : better ?
    };

    /**
     * Determine step type
     *
     * @param step
     * @returns {*}
     */
    this.getStepType = function(step) {
        if(step.classes && step.classes[0].isThesaurus()) {
            return "thesaurus";
        } else if(step.property.type=='data') {
            if(step.property.getRange().getUri() == "http://www.w3.org/1999/02/22-rdf-syntax-ns#langString") {
                return "langstring"
            }
            return "data";
        } else if(step.classes && step.classes[0].isSubClassOf(this.ontology.PointOfInterest, false)) {
            return "poi";
        } else if(step.classes && step.classes[0].isSubClassOf(this.ontology.PointOfInterestClass, false)) {
            return "type";
        } else {
            return "object";
        }
    };

    /**
     * Get transversal sub steps from range array
     *
     * @param step
     * @returns {*}
     */
    this.getSteps = function(classes, path) {
        var steps = {};
        if(!classes) {
            return steps;
        }

        var propertyDomains = {};

        // for each class
        for(var i=0; i<classes.length; i++) {
            var _class = classes[i];

            // get class transversal properties
            var properties = _class.getCandidateProperties();
            if(!properties || !properties.length) {
                continue;
            }

            for(var j=0; j<properties.length; j++) {
                var property = properties[j];

                // for each properties, get direct domains and perform filter
                var domains = this.getFilteredDomains(property, _class);

                // if no remaining domains, skip
                if(domains.length == 0) {
                    continue;
                }

                // add the property to the common list
                var propertyUri = property.getShortUri();
                if(!propertyDomains[propertyUri]) {
                    propertyDomains[propertyUri] = domains;
                } else {
                    propertyDomains[propertyUri] = propertyDomains[propertyUri].concat(domains);
                }
            }
        }

        // foreach collected property
        for(var uri in propertyDomains) {
            var property = this.ontology.getProperty(uri);

            // prepare step
            var step = {
                domains: propertyDomains[uri],
                property: property,
                path: path ? path + '/' + property.getShortUri() : property.getShortUri()
            };

            // build ranges
            var classes = [];
            var ranges = step.property.getRanges();
            for(var i=0; i<ranges.length; i++) {
                classes.push(ranges[i]);
            }

            if(classes.length > 0) {
                step.classes = classes;
            }

            // add to the steps list
            steps[uri] = step;
        }

        // return the list
        return steps;
    };

    /**
     * Filter domain list for a giver property
     * @param property
     */
    this.getFilteredDomains = function(property, _class) {
        // for each properties, get direct domains and perform filter
        var domains = property.getDirectDomains();

        // skip some domains
        return domains.filter(function(domain) {
            return !(
                // if domain is thesaurus class
                domain.isThesaurus()
                // if domain is not transversal with current class
                || !domain.isCandidateClassOf(_class)
                // if the domain is a POI class, different from the current class
                || (!domain.equals(_class) && domain.isSubClassOf(ctrl.ontology.PointOfInterest, false))
                // specific to root step : if the domain is not a direct superclass of the current class
                || _class.isSubClassOf(ctrl.ontology.PointOfInterest, false) && !_class.isSubClassOf(domain, true)
            );
        });
    };

    /**
     * Set definition data for given path
     *
     * @param path
     * @param data
     */
    this.setDefinitionRule = function(path, rule) {
        this.getDefinition().setRule(path, rule);
    };

    /**
     * Get definition data for given path
     *
     * @param path
     */
    this.getDefinitionRule = function(path) {
        return this.getDefinition().getRule(path);
    };

    /**
     * Get definition data for given path
     *
     * @param path
     */
    this.hasDefinitionRules = function(path) {
        return this.getDefinition().hasRules(path);
    };

    /**
     * Get definition status for given path
     *
     * @param path
     */
    this.getDefinitionStatus = function(path) {
        return this.getDefinition().getDefinitionStatus(path);
    };

    /**
     * Get the xpath context for given path
     *
     * @param path
     */
    this.getDefinitionXpathContext = function(path) {
        return this.getDefinition().getXpathContext(path);
    };

    this.preview = function() {

        // @see https://codepen.io/dimbslmh/pen/mKfCc
        bsNgDialog.open({
            fullHeight: true,
            template: require("./preview-dialog/preview-dialog.html"),
            plain: true,
            controller: 'PreviewDialogController',
            resolve: {
                definition: function() {
                    return ctrl.getDefinition();
                },
                _class: function() {
                    return ctrl.entrypoint;
                }
            }
        });
    }

    /**
     * Fix wrapper offset
     */
    function fixWrapperOffset() {
        var offset = 0;
        if(ctrl.steps.length > 0) {
            var width = $(".alignment-carousel > div").eq(0).outerWidth();
            offset = width * (ctrl.steps.length - 1);
        }
        ctrl.offset = offset;
    }

    /**
     * Normalize step argument
     *
     * @param arg
     * @returns {*}
     */
    function normalizeStepArg(arg) {
        if(typeof arg == "string") {
            for(var i=0; i<this.steps.length; i++) {
                if(this.steps[i]['path'] == arg) {
                    return this.steps[i];
                }
            }
        }
        return arg;
    }

    /**
     * Transform a path array to properties array
     *
     * @param path
     * @returns {any}
     */
    function pathToSteps(path) {
        var steps = [];
        var ranges = [ctrl.entrypoint];

        if(path) {
            var parts = path.split("/");
            path = [];
            while(parts.length > 0) {
                var part = parts.shift();
                var _path = path.join("/");

                var _steps = ctrl.getSteps(ranges, _path);
                if(!_steps || !_steps[part]) {
                    return $state.go('app.navigator', {uri: ctrl.entrypoint.getShortUri(), path: _path}); // @todo : better ?
                }

                // add substeps
                ranges = _steps[part].property.getRanges();
                _steps[part].steps = ctrl.getSteps(ranges, _steps[part].path);
                steps.push(_steps[part]);
                path.push(part);
            }

        }
        return steps;
    }

}
