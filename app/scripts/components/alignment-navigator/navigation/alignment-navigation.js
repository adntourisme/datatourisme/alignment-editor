/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('alignment-editor')
    .component('alignmentNavigation', {
        template: require("./alignment-navigation.html"),
        controller: controller,
        require: {
            navigator: "^alignmentNavigator"
        },
        bindings: {
            "ontology": "<",
            "stats": "<"
        }
    });

require("./alignment-navigation-item");

/**
 * @ngInject
 */
function controller(appConfig) {
    var ctrl = this;
    var hierarchy = [];

    this.$onChanges = function () {
        buildClassesHierarchy(this.ontology.PointOfInterest, hierarchy);
        this.hierarchy = hierarchy;
    };

    /**
     * Build a class hierarchy limited to classes
     *
     * @param res
     * @param obj
     */
    function buildClassesHierarchy(res, obj) {

        if(res.getUri().indexOf(appConfig.datatourisme.namespace) < 0) {
            return 0;
        }

        // get class transversal properties
        var properties = res.getCandidateProperties();
        if(!properties || !properties.length) {
            return 0;
        }

        var item = {
            res: res,
            subClasses: []
        };

        var count = 0;
        for(var j=0; j<properties.length; j++) {
            var property = properties[j];
            var domains = ctrl.navigator.getFilteredDomains(property, res);
            count += domains.length;
        }

        var subClasses = res.getSubClasses();
        for(var i in subClasses) {
            count += buildClassesHierarchy(subClasses[i], item.subClasses);
        }

        if(count > 0) {
            obj.push(item);
        }

        return count;
    }

}