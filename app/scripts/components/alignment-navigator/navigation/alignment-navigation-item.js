/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('alignment-editor')
    .component('alignmentNavigationItem', {
        template: require("./alignment-navigation-item.html"),
        controller: controller,
        require: {
            menu: "^alignmentNavigation"
        },
        bindings: {
            item: '<',
            level: '@'
        }
    });


/**
 * @ngInject
 */
function controller($rootScope, $state) {

    this.res = null;

    this.$onChanges = function () {
        // onchange, populate steps
        this.level = this.level ? parseInt(this.level) : 0;
        this.res = this.item.res;
    };

    this.cssClasses = function() {
        var classes = [];
        if(this.level > 0) {
            classes.push("list-group-item-lvl-" + this.level);
        }

        if($rootScope.entrypoint.hasUri(this.res.getUri())) {
            classes.push("active");
        }

        if(!this.getStat()) {
            classes.push("disabled");
        }

        return classes;
    }

    this.getStat = function() {
        var stat = this.menu.stats[this.res.getUri()];
        return stat ? stat : null;
    }

    this.visit = function() {
        if(this.getStat()) {
            $state.go('app.navigator', {uri: this.res.getShortUri(), path: null});
        }
    }

}