/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

var angular = require('angular');

angular
    .module('alignment-editor')
    .directive('windowHeightScroll', windowHeightScroll)

/**
 * @ngInject
 */
function windowHeightScroll($window, $timeout) {
    return {
        restrict: 'A',
        template: require("./window-height-scroll.html"),
        transclude: true,
        link: link
    };

    /**
     * @ngInject
     */
    function link(scope, element, attributes) {
        init();
        angular.element($window).bind('resize', init);
        function init() {
            $timeout(function() {
                //$(".nano", element).nanoScroller({ destroy: true });
                $(element).height(function(index, height) {
                    var offsetBottom = $(this).data('expand-to-bottom-offset');
                    return window.innerHeight - 20 - $(this).offset().top - (offsetBottom ? offsetBottom : 0);
                });
                $(".nano", element).nanoScroller();
            }, 100);
        }
    }

}
