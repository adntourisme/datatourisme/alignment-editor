/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
// cf https://github.com/umpirsky/language-list/blob/master/data/fr/language.txt

'use strict';

var angular = require('angular');

// service definition
angular
    .module('alignment-editor')
    .constant('iso639', [
        {"code": "fr", "label": "Français"},
        {"code": "en", "label": "Anglais"},
        {"code": "de", "label": "Allemand"},
        {"code": "nl", "label": "Néerlandais"},
        {"code": "it", "label": "Italien"},
        {"code": "es", "label": "Espagnol"},
        {"code": "ru", "label": "Russe"},
        {"code": "zh", "label": "Chinois"}
    ]);