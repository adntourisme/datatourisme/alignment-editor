/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';

/**
 * @param prefixMap
 * @constructor
 * @see https://jena.apache.org/documentation/javadoc/jena/org/apache/jena/shared/PrefixMapping.html
 */
function PrefixMapping(prefixMap) {
    var prefixMap = prefixMap ? prefixMap : {};

    /**
     * @returns {{}}
     */
    this.getPrefixMap = function() {
        return prefixMap;
    };

    /**
     * Answer the prefix for the given URI, or null if there isn't one.
     *
     * @param uri
     */
    this.getURIPrefix = function(uri) {
        if(uri) {
            for(var key in prefixMap) {
                if(uri.indexOf(prefixMap[key]) == 0) {
                    return key;
                }
            }
        }
        return null;
    };

    /**
     * Get the URI bound to a specific prefix, null if there isn't one.
     *
     * @param uri
     */
    this.getPrefixURI = function(prefix) {
        if(prefixMap[prefix]) {
            return prefixMap[prefix];
        }
        return null;
    };

    /**
     *
     * @param prefix
     * @param uri
     */
    this.setPrefix = function(prefix, uri) {
        prefixMap[prefix] = uri;
    };

    /**
     * Copies the prefixes from other into this.
     *
     * @param prefixes
     */
    this.setPrefixes = function(prefixes) {
        for(var prefix in prefixes) {
            this.setPrefix(prefix, prefixes[prefix]);
        }
    };

    /**
     * Expand the uri using the prefix mappings if possible.
     *
     * @param prefixed
     */
    this.expandPrefix = function(prefixed) {
        if(prefixed) {
            var pos = prefixed.indexOf(":");
            if(pos > -1 && prefixed.indexOf(" ") < 0) {
                var prefix = prefixed.substring(0, pos);
                if(prefixMap[prefix]) {
                    return prefixMap[prefix] + prefixed.substring(pos+1);
                }
            }
        }
        return prefixed;
    };

    /**
     * Compress the URI using the prefix mappings if possible.
     *
     * @param uri
     */
    this.shortForm = function(uri) {
        if(uri) {
            for(var key in prefixMap) {
                if(uri.indexOf(prefixMap[key]) == 0) {
                    return key + ':' + uri.substr(prefixMap[key].length);
                }
            }
        }
        return uri;
    };
}

module.exports = PrefixMapping;