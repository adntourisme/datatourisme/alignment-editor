/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */ 
'use strict';
var PrefixMapping = require('./PrefixMapping');

function AlignmentDefinition(data, ontology)
{
    var rule = angular.extend({}, {rules: {}}, data);
    var ontPrefixMap = new PrefixMapping(ontology.getPrefixes());
    var prefixMap = new PrefixMapping(data['@context']);

    /**
     * Normalize ontology -> alignement prefixed URI
     *
     * @param uri
     */
    function normalizeUri(uri) {
        var expandUri = ontPrefixMap.expandPrefix(uri);
        var prefix = prefixMap.getURIPrefix(expandUri);
        if(prefix === null) {
            prefix = ontPrefixMap.getURIPrefix(expandUri);
            var namespace = ontPrefixMap.getPrefixURI(prefix);
            if(namespace) {
                if(prefixMap.getPrefixURI(prefix)) {
                    // already exists
                    // create a new prefix ??
                } else {
                    prefixMap.setPrefix(prefix, namespace);
                }
            }
        }
        return prefixMap.shortForm(expandUri);
    }

    /**
     * Find rule by path
     * Normalize ontology -> alignement prefixed path
     *
     * @param uri
     */
    function findRuleByPath(path) {
        var target = rule;
        if(path) {
            var parts = path.split("/");
            for(var i=0; i<parts.length; i++) {
                var uri = normalizeUri(parts[i]);
                target.rules = target.rules ? target.rules : {};
                if(!target.rules[uri]) {
                    return null;
                }
                target = target.rules[uri];
            }
        }
        return target;
    }

    /**
     * URI short form, use normalisation
     *
     * @param uri
     */
    this.normalizeUri = normalizeUri;

    /**
     * Return all rules or sub rules
     *
     * @returns {}
     */
    this.getRules = function(path) {
        if(path) {
            var _rule = this.getRule(path);
            if(_rule && _rule.rules) {
                return _rule.rules;
            }
            return null;
        } else {
            return rule.rules;
        }
    };

    /**
     * @returns {{}}
     */
    this.getPrefixMap = function() {
        return prefixMap.getPrefixMap();
    };

    /**
     * toJson
     */
    this.toJson = function() {
        var json = angular.copy(rule);
        json["@context"] = prefixMap.getPrefixMap();
        return json;
    };

    /**
     * Set rule for a given uri and path
     *
     * @param uri
     * @param path
     * @param data
     */
    this.setRule = function(path, rule) {
        var parts = path.split("/");
        var uri = normalizeUri(parts.pop());

        var parent = findRuleByPath(parts.join("/"));

        if(parent) {
            if(rule) {
                parent.rules = parent.rules ? parent.rules : {};
                parent.rules[uri] = rule;
            } else {
                if(parent.rules) {
                    delete parent.rules[uri];
                }
            }
        }
    };

    /**
     * Get rule for a given uri and path
     *
     * @param uri
     * @param path
     * @param data
     */
    this.getRule = function(path) {
        var rule = findRuleByPath(path);
        return rule ? rule : null;
    };

    /**
     * Get rule for a given uri and path
     *
     * @param uri
     * @param path
     * @param data
     */
    this.hasRules = function(path) {
        var rule = findRuleByPath(path);
        return rule && rule.rules && Object.keys(rule.rules).length > 0;
    };

    /**
     * Get rule for a given uri and path
     *
     * @param uri
     * @param path
     * @param data
     */
    this.setRules = function(path, rules) {
        var rule = findRuleByPath(path);
        if(rule) {
            rule.rules = rules;
        }
    };

    /**
     * Get status for a given uri and path
     *
     * @param uri
     * @param path
     * @param data
     *
     * @return false : definition doesn't exists
     * @return true : definition exists
     */
    this.getDefinitionStatus = function(path) {
        var rule = findRuleByPath(path);
        if(rule) {
            return typeof rule.rules == "undefined" || Object.keys(rule.rules).length > 0;
        }
        return false;
    };

    /**
     * Return the active xpathContext for the given path
     *
     * @param uri
     * @param path
     */
    this.getXpathContext = function(path) {
        var parts = path.split("/");
        parts.pop();
        var context = [];

        while(parts.length > 0) {
            var subpath = parts.join("/");
            var rule = findRuleByPath(subpath);
            if(rule && rule.expr) {
                context.unshift(rule.expr);
            }
            parts.pop();
        }

        return context ? context : null;
    };

    /**
     * @param obj
     * @returns {boolean}
     */
    function isEmpty(obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }
        return true;
    }
}

module.exports = AlignmentDefinition;